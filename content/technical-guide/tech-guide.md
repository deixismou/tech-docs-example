---
title: "Technical Guide Document"
date: 2019-09-09T23:20:35+10:00
draft: false
---
<center><h2>How to use GitLab and Hugo to publish technical documentation online</h2></center>

## Document overview

{{% notice note %}}
This guide covers the installation and configuration process only; for
information on editing and managing content for a Hugo site, refer to the
official documentation listed in additional resources.
{{% /notice %}}

This technical guide describes the configuration and deployment of an online
documentation website using GitLab’s free static hosting service, GitLab Pages,
and the Hugo static site generator. Hugo’s markdown-centric workflow and minimal
dependencies make it ideal for building rapid documentation projects on-the-fly,
while maintaining version control and managing collaboration with Git.

Pairing this workflow with GitLab’s built-in continuous integration, issue
tracking and collaboration tools provides an ideal platform for collaborative
documentation projects, from simple install notes to larger, more critical
documentation projects.

You can find an live example site at: https://deixismou.gitlab.io/tech-docs-example  
And the accompanying source code at: https://gitlab.com/deixismou/tech-docs-example 

The project created in this guide will include the following features:

##### Rich markdown-centric editing
Hugo’s markdown engine provides a content-rich, yet platform independent system
for content creation and editing.

##### Git version control and collaboration
The collaborative workflow and deep version control management provided by Git
is an industry-standard way to manage source-code. With documentation content in
markdown (i.e. plaintext), it can also be used to track documentation content.

##### Continuous integration
With GitLab’s built-in continuous integration tools, documentation changes can
be published to the website automatically, whenever changes are pushed to the
main branch of the repository. No need for manual builds.

## Prerequisites
This guide assumes you are working from a Unix-based system (Mac OSX or Linux), however a similar process can be followed for Windows. Refer to the Additional Resources section of this guide for documentation on configuring Hugo and Git for Windows environments using Bash for Windows.

### Software Requirements

1.	A suitable text editor (e.g. Atom, SublimeText, Emacs or Vim)
2.	A locally installed version of Git
3.	The Hugo static site generator (see installation section)
4.	A user account at https://gitlab.com/ 

## Installing Hugo
Hugo is built using the programming language Go, and will run anywhere the Go
toolchain can run. In other words, it is platform independent. However, the
installation options will depend on your operating system and package manager.

On most Linux systems, you can install it directly from the command line using
the package manager:

```bash
# debian / ubuntu
$ sudo apt install hugo

# Archlinux
$ sudo pacman -S hugo
```
 
Mac OSX users can install it from the command line using homebrew:

```bash
$ brew install hugo
```
 
Alternatively, you can download a self-contained binary from
https://github.com/gohugoio/hugo/releases and save it anywhere within your
executable `PATH`.

To test that the installation is successful, open a terminal and enter the
following command:

```bash
$ hugo version
Hugo Static Site Generator v0.57.2/extended linux/amd64 BuildDate: unknown
```
 
## Configuring the local website instance
The hugo binary provides sub-commands for generating new content and viewing the
generated files. To create a new site, open a terminal in the parent directory
where you wish to save your site content, and enter the following command:

```bash
$ hugo new site documentation-site
Congratulations! Your new Hugo site is created…
```
 
The new site command will create a directory with the following file structure.

```bash
documentation-site
├── archetypes
│   └── default.md
├── config.toml
├── content
├── data
├── layouts
├── static
└── theme
```
 
For now, most of the directories in the site folder are empty, since no content
has been created yet. Before the site can be viewed on the local machine, you
must install a suitable theme. This example will use the Learn theme, which is
designed specifically for online documentation.

Before installing the theme, initiate a git repository in the site’s root
directory. 

```bash
$ git init
Initialized empty Git repository …
$ echo "/public" > .gitignore
$ git add .
$ git commit -m ‘initial commit’
```
 
The Learn theme is available from GitHub. To clone the theme directly into you
site as a Git submodule, execute the following command from the root directory
of the site.

```bash
$ git submodule add https://github.com/matcornic/hugo-theme-learn.git > themes/learn
Cloning into '/home/deixismou/workspace/documentation-example-site/themes/learn'
...
```
 
This will clone the Learn theme into the themes directory of your Hugo site.
However, in order to activate the theme, you need to include it in the
config.toml file that Hugo created in the site’s root directory. This is a good
time to setup other settings, such as the site title. Open config.toml in your
text editor and edit it to reflect the following information.

```toml
baseURL = "http://example.org/"
languageCode = "en-au"
title = "My Documentation Site"fault theme to be use when building the 
theme = "learn"# For search functionality

[outputs]
home = [ "HTML", "RSS", "JSON"]
```
  
You can now create a test page and review the site in the browser. First,
generate some site content using the following commands.

```bash
$ hugo new _index.md
$ hugo new --kind chapter introduction/_index.md
$ hugo new introduction/what-is-technical-documention.md
$ hugo new introduction/types-of-technical-documention.md
```
 
These commands will generate new markdown templates in the content directory of
your site. With this content in-place, you can now start Hugo’s built-in
development server.

```bash
$ hugo serve -D

```
 
By default, the server creates a service running on http://localhost:1313.
Leaving the server running, you can now open your web browser to that URL and
view the generated site. Any changes made to the markdown files in your content
directory will be automatically updated in the browser.

## Configuring GitLab CI and Pages

This example will use GitLab’s continuous integration tools and static hosting
platform to manage automated deployment of the documentation site. This will
allow you to edit locally and push new content to the site using git as the
primary deployment tool.

Create a new public project within your GitLab account and follow the usual
method to add an existing git repository to it. From the site’s root directory
on your local machine, execute the following.

```bash
$ git remote rename origin old-origin
$ git remote add origin git@gitlab.com:{username}/{project-name}.git
$ git push -u origin --all
$ git push -u origin –tags
```

{{% notice note %}}
Replace {username} and {project-name} with your actual repository info.
{{% /notice %}}
 
GitLab’s CI toolchain is configured from within your Git repository, by
committing and pushing a configuration file named `.gitlab-ci.yml` in the root
directory of your repository. Create the file and paste the following contents.

```yml
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

{{% notice note %}}
Mind the leading dot in the filename of `.gitlab-ci.yml`
{{% /notice %}}
 
With the `.gitlab-ci.yaml` file created, add it to your repository and commit the
changes.

```bash
$ git add .gitlab-ci.yml
$ git commit -m 'create gitlab-ci config'
[master 4e0651c] create gitlab-ci config
 1 file changed, 13 insertions(+)
 create mode 100644 .gitlab-ci.yml
```
 
Finally, edit config.toml to reflect the base url of your site as it will appear
on GitLab Pages.

```toml
# config.toml
baseURL = http://{username}.gitlab.io/{project-name}/
# …

```
 
Now push the repository and wait for GitLab to build your site. It may take
20-30 minutes for the site to appear. Eventually, it will be available publicly
via the base URL.   

## Additional Resources

##### Hugo Official Documentation

https://gohugo.io/documentation/   
Configuration: https://gohugo.io/getting-started/configuration/   
Hosting on GitLab: https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/  

##### GitLab Official Documentation
https://docs.gitlab.com/  
Gitlab Pages: https://docs.gitlab.com/ee/user/project/pages/  
GitLab CI: https://docs.gitlab.com/ee/ci/README.html  

##### Lean Theme Documentation
https://learn.netlify.com/en/  
https://github.com/matcornic/hugo-theme-learn  


---
title: "An Example Documentation Site"
date: 2019-09-09T23:20:35+10:00
draft: false
---

# An Example Documentation Site

This site was created to illustrate a technical documentation assignment for
the Certificate IV in Programming at the Canberra Institute of Technology. Feel
free to browse the example documentation provided.

This site is currently hosted for free on [GitLab.com](https://GitLab.com), the
source code is availiable [in a public
repository](https://gitlab.com/deixismou/tech-docs-example/) for reference.

For information on how to do this, see the 
[technical Guide Example](http://localhost:1313/tech-docs-example/technical-guide/)

![Hugo Gopher](https://d33wubrfki0l68.cloudfront.net/d7c79b5c53384a57cfcf5bfb1a3f6f009a058b0b/16f81/images/gopher-hero.svg?width=200px)
![GitLab Logo](https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png?width=250px)
